<?php

namespace App\Form;

use App\Entity\Film;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;

class FilmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('releaseDate', Type\DateType::class, array(
                'widget' => 'choice',
                'years' => range(date('Y') - 50, date('Y') + 10)
            ))
            ->add('type')
            ->add('director')
            ->add('viewingDate', Type\DateType::class, array(
                'widget' => 'choice',
                'years' => range(date('Y') - 5, date('Y') + 1)
            ))
            ->add('view', Type\ChoiceType::class, array(
                'choices' => range(0, 20, 1)
            ))
            ->add('submit', Type\SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Film::class,
            'csrf_protection' => false,
        ]);
    }
}
