<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmType;
use App\Repository\FilmRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type as Type;

/**
 * @Route("/film", name="film_")
 */
class FilmController extends AbstractController
{
    private $filmrepository;

    public function __construct(FilmRepository $filmRepository)
    {
        $this->filmrepository = $filmRepository;
    }

    /**
     * @Route("/", name="index")
     */
    public function index(): Response
    {
        $films = $this->filmrepository->findAll();
        return $this->render('film/index.html.twig', [
            'films' => $films,
        ]);
    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @Route("/add", name="add")
     */
    public function addFilm(Request $request, EntityManagerInterface $entityManager): Response
    {
        $film = new Film();
        $form = $this->createForm(FilmType::class, $film);
        //$form->add('Add', Type\SubmitType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $entityManager->persist($film);
            $entityManager->flush();

            return $this->redirectToRoute('film_index');
        }

        return $this->render('film/add.html.twig', [
            'film_form' => $form->createView(),
        ]);

    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param Film $film
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/delete/{id}", name="delete", methods={"POST"})
     */
    public function deleteFilm(Request $request, EntityManagerInterface $entityManager, Film $film): Response
    {
        if ($this->isCsrfTokenValid('delete'.$film->getId(), $request->request->get('_token'))) {
            $entityManager->remove($film);
            $entityManager->flush();
        }

        return $this->redirectToRoute('film_index', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("/film/{id}/edit", name="edit", methods={"GET", "POST"})
     */
    public function editFilm(Request $request, EntityManagerInterface $entityManager, Film $film)
    {
        $form = $this->createForm(FilmType::class, $film);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('film_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('film/edit.html.twig', [
            'film' => $film,
            'film_form' => $form,
        ]);
    }
}
